#importing necessary libraries
import numpy as np
import pandas as pd

def getNeighbors(base, random, k, weights): #Get the K most similar houses to the one selected but
    distances = []                          #taking into account the different weights of all the dimensions
    for index, row in base.iterrows():
        dist = np.linalg.norm(random - np.dot(row, np.transpose(weights)))
        distances.append((row, dist))
    distances.sort(key=lambda x: x[1])
    neighbors = []
    for x in range(k):
        neighbors.append(distances[x][0])
    return neighbors

def updateWeights(house1, house2, weights): #Update the weights giving more importance to the ones with less variance
    learning_rate = 0.1
    house1 = house1.to_numpy()
    house2 = house2.to_numpy()
    variance = house2 - house1
    weights = weights * (1-learning_rate) + learning_rate * variance
    return abs(weights)

def update(newDatabase): #Update the database (regular and normalized)
    global df
    global normalized
    df = newDatabase
    normalized = (df-df.min())/(df.max()-df.min())


#Initialize variables 
df = pd.read_csv('ParisHousingClass.csv')
df = df.drop(['category'], axis=1)
normalized = (df-df.min())/(df.max()-df.min())
history = []

#Get first sample randomly
random = normalized.sample(n=1)

#Array of ones of length 17
weights = np.ones(len(df.columns))

while True:
    neighbors = getNeighbors(normalized, random, 5, weights)
    ids = []
    for i in range(0,5):
        ids.append(neighbors[i].name)
    print(ids)
    #Get user keyboard input as int
    print("\n Please enter a number from 1 to 5")
    user_input = int(input())-1

    weights = updateWeights(random, neighbors[user_input], weights)
    random = neighbors[user_input]
    ids.pop(user_input)
    normalized = normalized.drop(ids)





    



