#importing necessary libraries
import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

df = pd.read_csv('ParisHousingClass.csv')
df = df.drop(['category'], axis=1)
print(df)
print(df.loc[1000])
#Get the most similar house
random = df.sample(n=1)


#Get the most similar house
random = df.sample(n=1)

while True:
    #Get the most similar house
    cosine_sim = cosine_similarity(df, df)
    print(cosine_sim)
    cosine_sim = pd.DataFrame(cosine_sim, columns=df.index.values, index=df.index.values)
    cosine_sim = cosine_sim.sort_values(by=random.index.values[0], ascending=False)
    neighbors = cosine_sim.index.values[1:6]
    neighbors = df.loc[neighbors,:]
    print(neighbors)
    #Ask user number from 1 to 5
    print("Please enter a number from 1 to 5")
       
    #Get user keyboard input as int
    user_input = int(input())-1

    #Update the selected house
    random = neighbors.iloc[user_input,:]

    print("Do you want to continue? (y/n)")
    user_input = input()
    if user_input == 'n':
        break
    else:
        continue

