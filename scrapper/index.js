const scrapper = require('./scrapper')
const mongo = require('./mongo')

async function updatedb() {
    const houses = await scrapper.getData();
    await mongo.updatedb(houses);
}

updatedb()