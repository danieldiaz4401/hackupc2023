const axios = require('axios');
const cheerio = require('cheerio');

const url = 'https://www.pisos.com/alquiler-residencial/pisos-barcelona_capital/';

async function getData() {

    let max;
    let result = [];

    let houses = [];

    await axios.get(url)
        .then(response => {
            const html = response.data;
            const $ = cheerio.load(html);
            const sentence = $('.pagination__counter').text();

            let regex = /\d+/g;

            let match = sentence.match(regex);
            max = match[1];

            houses = houses.concat(getHouses($));

            console.log("Updating data: " + houses.length + "/" + max)
        })
        .catch(error => console.error(error));

    let i = 2;
    while (houses.length < max && i < 300) {
        await axios.get(`${url}${i}`)
            .then(response => {
                const html = response.data;
                const $ = cheerio.load(html);
                houses = houses.concat(getHouses($));
                console.log("Updating data: " + houses.length + "/" + max)
            })
            .catch(error => console.error(error));
        i++;
    }
    return houses;
}

function getHouses($) {
    const items = $('.ad-preview');
    const list = []

    items.each((i, e) => {
        let item = {};
        item.link = `https://www.pisos.com/${e.attribs["data-lnk-href"].substring(1)}`;
        item.id = e.attribs["id"];
        item.cost = parseInt($(e).find(".ad-preview__price").text().trim().replace(".", "").split(" ")[0]);
        list.push(item);
    })
    return list;
}


exports.getData = getData;