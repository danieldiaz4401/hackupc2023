require('dotenv').config()
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

dbUser = process.env.DATABASE_USER
dbPassword = process.env.DATABASE_PASSWORD
dbCluster = process.env.DATABASE_CLUSTER

const csvFilePath="./output.csv";

const { MongoClient, ServerApiVersion } = require('mongodb');


const uri = `mongodb+srv://${dbUser}:${dbPassword}@cluster0.twq2kdv.mongodb.net/?retryWrites=true&w=majority&authMechanism=SCRAM-SHA-1`;

const collectionName = "flats-ialista";

async function updatedb(data) {
  const client = new MongoClient(uri);
  try {

    await exportCSV(data);

    // Get the reference to the database
    const database = client.db(dbCluster);

    // Get the reference to the collection
    const collection = database.collection(collectionName);

    // Delete all data
    await collection.deleteMany({})

    // Update all data
    await collection.insertMany(data);


  } catch (error) {
    console.error('Error updating MongoDB data:', error);
  } finally {
    // Close the connection
    await client.close();
  }
}

async function exportCSV(data) {

  const csvWriter = createCsvWriter({
    path: csvFilePath,
    header: Object.keys(data[0]), // Assuming the first document represents the headers
  });

  await csvWriter.writeRecords(data);
}

exports.updatedb = updatedb;